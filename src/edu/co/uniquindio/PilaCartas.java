package edu.co.uniquindio;

public class PilaCartas {

    private NodoCarta cabeza;

    public PilaCartas() {
    }

    public void insertar(int pinta, String valor) {
        if (cabeza == null) {
            cabeza = new NodoCarta(pinta, valor);
        }
        else {
            NodoCarta actual = cabeza;

            while(actual.getSiguiente() != null) {
                actual = actual.getSiguiente();
            }
            actual.setSiguiente(new NodoCarta(pinta, valor));
        }
    }

    public NodoCarta buscar(int pinta, String valor) {
        NodoCarta actual = cabeza;
        if (cabeza != null) {
            do {
                if (actual.getPinta() == pinta && actual.getValor().equals(valor)) {
                    return actual;
                }
                actual = actual.getSiguiente();
            }
            while(actual != null);
        }
        return null;
    }
}
