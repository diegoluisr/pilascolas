package edu.co.uniquindio;

public class PilaCartasRecursico {

    private NodoCartaRecursivo cabeza;

    public PilaCartasRecursico() {
    }

    public void insertar(int pinta, String valor) {
        if (cabeza == null) {
            cabeza = new NodoCartaRecursivo(pinta, valor);
        }
        else {
            cabeza.insertar(pinta, valor);
        }
    }

    public NodoCartaRecursivo buscar(int pinta, String valor) {
        if (cabeza == null) {
            return null;
        }
        return this.cabeza.buscar(pinta, valor);
    }
}
