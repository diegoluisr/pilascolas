package edu.co.uniquindio;

public class Main {

    public static void main(String[] args) {

        PilaCartas pila = new PilaCartas();
        pila.insertar(NodoCarta.CORAZONES, "1");
        pila.insertar(NodoCarta.CORAZONES, "3");
        pila.insertar(NodoCarta.CORAZONES, "5");
        pila.insertar(NodoCarta.CORAZONES, "7");
        pila.insertar(NodoCarta.CORAZONES, "9");
        pila.insertar(NodoCarta.CORAZONES, "J");

        System.out.println(pila.buscar(NodoCarta.CORAZONES, "3"));

        PilaCartasRecursico pilaRecursiva = new PilaCartasRecursico();
        pilaRecursiva.insertar(NodoCarta.CORAZONES, "1");
        pilaRecursiva.insertar(NodoCarta.CORAZONES, "3");
        pilaRecursiva.insertar(NodoCarta.CORAZONES, "5");
        pilaRecursiva.insertar(NodoCarta.CORAZONES, "7");
        pilaRecursiva.insertar(NodoCarta.CORAZONES, "9");
        pilaRecursiva.insertar(NodoCarta.CORAZONES, "J");

        System.out.println(pilaRecursiva.buscar(NodoCarta.CORAZONES, "3"));

        /*Pila<String> pila = new Pila<>();
        pila.insertar(new Nodo("Hola"));
        pila.insertar(new Nodo("Mundo"));
        pila.insertar(new Nodo("Elemento a quitar"));
        System.out.println(pila);
        System.out.println(pila.quitar());
        System.out.println(pila);

        Cola<String> cola = new Cola<>();
        cola.insertar(new Nodo("Elemento a quitar"));
        cola.insertar(new Nodo("Hola"));
        cola.insertar(new Nodo("Mundo"));
        System.out.println(cola);
        System.out.println(cola.quitar());
        System.out.println(cola);*/

    }
}
