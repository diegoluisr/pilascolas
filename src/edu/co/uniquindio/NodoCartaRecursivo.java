package edu.co.uniquindio;

public class NodoCartaRecursivo {

    private NodoCartaRecursivo siguiente;
    private int pinta;
    private String valor;

    final static int PICAS = 1;
    final static int CORAZONES = 2;
    final static int DIAMANTES = 3;
    final static int TREBOLES = 4;

    public NodoCartaRecursivo(int pinta, String valor) {
        this.pinta = pinta;
        this.valor = valor;
    }

    public NodoCartaRecursivo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoCartaRecursivo siguiente) {
        this.siguiente = siguiente;
    }

    public int getPinta() {
        return pinta;
    }

    public String getValor() {
        return valor;
    }

    public NodoCartaRecursivo buscar(int pinta, String valor) {
        if (this.getPinta() == pinta && this.getValor().equals(valor)) {
            return this;
        }
        else if (this.getSiguiente() != null) {
            return this.getSiguiente().buscar(pinta, valor);
        }
        return null;
    }

    public void insertar(int pinta, String valor) {
        if (this.getSiguiente() != null) {
            this.getSiguiente().insertar(pinta, valor);
        }
        else {
            this.setSiguiente(new NodoCartaRecursivo(pinta, valor));
        }
    }
}
