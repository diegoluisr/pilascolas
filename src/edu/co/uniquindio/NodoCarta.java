package edu.co.uniquindio;

public class NodoCarta {

    private NodoCarta siguiente;
    private int pinta;
    private String valor;

    final static int PICAS = 1;
    final static int CORAZONES = 2;
    final static int DIAMANTES = 3;
    final static int TREBOLES = 4;

    public NodoCarta(int pinta, String valor) {
        this.pinta = pinta;
        this.valor = valor;
    }

    public NodoCarta getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoCarta siguiente) {
        this.siguiente = siguiente;
    }

    public int getPinta() {
        return pinta;
    }

    public String getValor() {
        return valor;
    }
}
